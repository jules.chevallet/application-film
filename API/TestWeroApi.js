const API_TOKEN = "keypgd1aZW1Gb2RQP";
var Airtable = require('airtable');
Airtable.configure({
    endpointUrl: 'https://api.airtable.com',
    apiKey: API_TOKEN
});

var base = Airtable.base('appjols9AyfYMKxdr');
var View = "All employees";
var sub_base = 'Refugie';
var is_Registered = false
var id = 'recLsmxkXZ9TXSoeC'


// Ici on va lister tous les identifiants Airtable des réfugiés, qui représentent la clé primaire de la database

// base(sub_base).select({ view: View}).eachPage(
//     function page(records, fetchNextPage) {
//         records.forEach(function(record) {
//             //Ici on attribue des mots de passe arbitraires aux réfugiés, afin d'exécuter un test d'authentification
//             base(sub_base).update(record.id,{
//                 "id": record.id,
//                 "Mots de passe": 'azer' + record.id.slice(4,6)
//             })
//             console.log('id=', record.get('id'))
//         });
//         fetchNextPage();
//     }, 
//     function done(err) {
//         if (err) { console.error(err); return; }
//     }
// );

//Test de vérification de présence de l'utilisateur dans la database
//Repose sur un callback (fn) pour récupérer la valeur du test d'erreur
//fn est défini au moment du call de _isMember
//cette méthode pourra à l'avenir être simplifié
function _isRegistered(id, fn) {
    base(sub_base).find(id, function(err, record) {
        if (err) {
            isRegistered = false; 
            fn(isRegistered); 
        }
        else {
            isRegistered = true
            fn(isRegistered)
        }
    })
}

// Test de vérification de l'inscription de l'utilisateur
export function _isMember(user_id, fn_On_This) {
    filter_1 = '{id} = "recLsmxkXZ9TXSoeC"'
    // filter_2 = filter_1.replace("o", '');
    base(sub_base).select({
        view: View,
        filterByFormula: filter_1
    }).eachPage(function page(records, fetchNextPage) { 
        records.forEach(fn_On_This(record));
        fetchNextPage();
    }, function done(err) {
        if (err) { console.error(err); return; }
    });
}

//Attribution d'un mot de passe pour un utilisateur spécifique (à inclure dans un/des component(s))
export function _allocatePassword(id) {
    base(sub_base).update(id, {
        "Mots de passe": 'azer' + id.slice(7,9)
    })
    base(sub_base).find(id, function(err, record) {
        if (err) { console.error(err); return; }
        // console.log('MDP =', record.get('Mots de passe'));
    });
    return
}

//Test


let records = []
const processPage = (partialRecords, fetchNextPage) => {
  records = [...records, ...partialRecords]
  fetchNextPage()
}
const processRecords = (err) => {
  if (err) {
    console.error(err)
    return
  }

}
base(sub_base).select({
    view: View
}).eachPage(processPage, processRecords)

console.log(records)

// _allocatePassword (id)

// _isRegistered(id, function(isRegistered) {console.log(isRegistered)})
// console.log(is_Registered)
// _isMember(id)

